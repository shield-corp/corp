# Corp

[![pipeline status](https://forge.univ-lyon1.fr/shield/Corp/badges/master/pipeline.svg)](https://forge.univ-lyon1.fr/shield/Corp/commits/master)

![Logo](./data/images/icon.png)

[Documentation](https://shield-corp.gitlab.io/corp/)

## Contributing

### Getting started

- You need `git` installed
- Fork the repo
- `git clone https://forge.univ-lyon1.fr/yourusername/Corp.git`
- `cd Corp`
- `mkdir tmp` **Saves will be saved here**
- `git submodule update --init`
- `sudo apt-get install libboost-all-dev libfreetype6-dev libjpeg-dev libopenal-dev libalut-dev libvorbis-dev` or equivalent for 
your linux distro
- `git remote add upstream https://forge.univ-lyon1.fr/shield/Corp.git`

### Coding a feature / bug fix...

- `git checkout -B task/subject`
- `git commit myfiles -m "Description of what I did"`
- `git push origin task/subject`
- Go to gitlab and create a merge request

Once your commit is merged do that :
- `git checkout master`
- `git branch -D task/subject`
- `git pull upstream master`

## Editors

### CLion

- Setup the `Google` formatting style for the project in CLion

- Add catch config for easy testing

![Catch config example](./doc/catch-config.png)

### Command line

- Type `cmake -H. -Bbuild && make -C build` for an out of source build

## External libraries

External libraries are dealt in two ways in this project :
- There are **header only libraries** located in the `include/` library
- There are more **complex libraries** located in `vendor/` and fetched using `git submodules --init`

## Troubleshooting

If needed install :
 - SDL2 : `apt://libsdl2-dev`

## Smart pointers

This project heavily relies on `boost::shared_ptr` and `oxygine::intrusive_ptr` so that resource don't have to be freed manually.

Thus all modules have their smart pointer version, for example :
`Game` and `spGame`.
