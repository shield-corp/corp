#include <catch.hpp>
#include "../../src/core/CharacterRepository.h"
#include "../../src/core/characters/Agent.h"

using namespace core;

TEST_CASE("when CharacterRepository is created") {
  SECTION("it should work for constructor/0") {
    CharacterRepository character_repository;
  }
}

TEST_CASE("when CharacterRepository loads characters") {
  SECTION("it should instanciate a character") {
    CharacterRepository character_repository;

    character_repository.load("../test/data/characters/");

    auto characters = character_repository.fetch_all();

    REQUIRE(characters.size() == 2);
  }
}

TEST_CASE("when CharacterRepository get level MAX for characters") {
  SECTION("it should return a levelmax of character") {
    CharacterRepository character_repository;

    character_repository.load("../test/data/characters/");

    auto levelmax = character_repository.fetch_level_max();

    REQUIRE(levelmax > 0);
  }
}

TEST_CASE("when CharacterRepository get level occurency for characters") {
  SECTION("it should return a level occurency of all characters") {
    CharacterRepository character_repository;

    character_repository.load("../test/data/characters/");

    auto level_occurency = character_repository.get_level_occurrences();

    REQUIRE(level_occurency.at(4) > 0);
  }
}
