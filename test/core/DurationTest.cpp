#include "../../include/catch.hpp"
#include "../../src/core/Duration.h"

using namespace core;

TEST_CASE("when Duration is created") {
  SECTION("it should work for constructor/0") {
    Duration duration;

    REQUIRE(duration.get_days() == 0);
  }
  SECTION("it should work for constructor/1") {
    unsigned int days = 10;
    Duration duration(days);

    REQUIRE(duration.get_days() == days);
  }
}
