#include "../../include/catch.hpp"
#include "../../src/core/Feed.h"

using namespace core;

TEST_CASE("when Feed exceed its size") {
  SECTION("it should pop old elements/1") {
    spFeed feed = spFeed(new Feed);

    list<string> messages = {
        "Test",
        "Test2",
        "Test3",
        "Test4",
        "Test5",
        "Test6"
    };
    list<string> expected_messages = {
        "Test2",
        "Test3",
        "Test4",
        "Test5",
        "Test6"
    };

    for(const auto &message : messages) {
      feed->push(message);
    }

    REQUIRE(feed->fetch_all() == expected_messages);
  }
}
