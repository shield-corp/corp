#define CATCH_CONFIG_MAIN
#include "../include/catch.hpp"

#include <string>

CATCH_TRANSLATE_EXCEPTION(std::string &ex) {
  return ex;
}
