#include "Hero.h"
#include "../rules.h"

namespace core {
namespace characters {

unsigned int Hero::get_agility() const { return agility; }
void Hero::set_agility(unsigned int agility) { Hero::agility = agility; }
unsigned int Hero::get_charisma() const { return charisma; }
void Hero::set_charisma(unsigned int charisma) { Hero::charisma = charisma; }
unsigned int Hero::get_ingenuity() const { return ingenuity; }
void Hero::set_ingenuity(unsigned int ingenuity) {
  Hero::ingenuity = ingenuity;
}

std::ostream &operator<<(std::ostream &out, const Hero &hero) {
  out << "Hero( " << hero.get_name() << ", " << hero.get_surname() << ")";

  return out;
}

bool Hero::operator==(const Hero &hero) const {
  return (Character) * this == (Character)hero && agility == hero.agility &&
         charisma == hero.charisma && ingenuity == hero.ingenuity;
}

void Hero::set_stats() {
  switch (this->level) {
    case 1:
      set_xp_max(10);
      this->set_wage(10);
      this->set_agility(10);
      this->set_destruction_ability(10);
      this->set_ingenuity(10);
      this->set_charisma(10);
      break;
    case 2:
      set_xp_max(20);
      this->set_wage(15);
      this->set_agility(20);
      this->set_destruction_ability(20);
      this->set_ingenuity(15);
      this->set_charisma(20);
      break;
    case 3:
      set_xp_max(40);
      this->set_wage(20);
      this->set_agility(30);
      this->set_destruction_ability(30);
      this->set_ingenuity(20);
      this->set_charisma(30);
      break;
    case 4:
      set_xp_max(80);
      this->set_wage(30);
      this->set_agility(40);
      this->set_destruction_ability(45);
      this->set_ingenuity(25);
      this->set_charisma(35);
      break;
    case 5:
      set_xp_max(160);
      this->set_wage(40);
      this->set_agility(45);
      this->set_destruction_ability(50);
      this->set_ingenuity(30);
      this->set_charisma(40);
      break;
    case 6:
      set_xp_max(320);
      this->set_wage(50);
      this->set_agility(50);
      this->set_destruction_ability(55);
      this->set_ingenuity(35);
      this->set_charisma(45);
      break;
    case 7:
      set_xp_max(640);
      this->set_wage(70);
      this->set_agility(55);
      this->set_destruction_ability(60);
      this->set_ingenuity(40);
      this->set_charisma(50);
      break;
    case 8:
      set_xp_max(1280);
      this->set_wage(90);
      this->set_agility(60);
      this->set_destruction_ability(65);
      this->set_ingenuity(45);
      this->set_charisma(55);
      break;
    case 9:
      set_xp_max(2600);
      this->set_wage(120);
      this->set_agility(65);
      this->set_destruction_ability(70);
      this->set_ingenuity(50);
      this->set_charisma(60);
      break;
    case 10:
      set_xp(0);
      set_xp_max(0);
      this->set_wage(150);
      this->set_agility(75);
      this->set_destruction_ability(80);
      this->set_ingenuity(60);
      this->set_charisma(75);
      break;
    default:
      set_level(1);
      break;
  }
}
void Hero::level_update() {
  if (level < LEVEL_MAX) {
    if (xp >= xp_max) {
      xp = this->get_xp() - this->get_xp_max();
      this->set_level((this->level) + 1);
      this->set_stats();
    }
  }
}
unsigned int Hero::get_power() const {
  return get_agility() + get_agility() + get_charisma() +
         get_destruction_ability();
}
}  // namespace characters
}  // namespace core
