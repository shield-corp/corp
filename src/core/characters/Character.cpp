#include "Character.h"

namespace core {
namespace characters {

const string &Character::get_name() const { return name; }
void Character::set_name(const string &name) { Character::name = name; }
const string &Character::get_surname() const { return surname; }
void Character::set_surname(const string &surname) {
  Character::surname = surname;
}
const string &Character::get_description() const { return description; }
void Character::set_description(const string &description) {
  Character::description = description;
}
unsigned int Character::get_level() const { return level; }
void Character::set_level(unsigned int level) { Character::level = level; }

unsigned int Character::get_xp() const { return xp; }
void Character::set_xp(unsigned int xp) { Character::xp = xp; }
unsigned int Character::get_xp_max() const { return xp_max; }
void Character::set_xp_max(unsigned int xp_max) { Character::xp_max = xp_max; }
bool Character::get_available() const { return available; }
void Character::set_available(bool available) {
  Character::available = available;
}
const DateTime &Character::get_indisponibility_start() const {
  return indisponibility_start;
}
void Character::set_indisponibility_start(
    const DateTime &indisponibility_start) {
  Character::indisponibility_start = indisponibility_start;
}
const Duration &Character::get_indisponibility_duration() const {
  return indisponibility_duration;
}
void Character::set_indisponibility_duration(
    const Duration &indisponibility_duration) {
  Character::indisponibility_duration = indisponibility_duration;
}
unsigned int Character::get_wage() const { return wage; }
void Character::set_wage(unsigned int wage) { Character::wage = wage; }
unsigned int Character::get_destruction_ability() const {
  return destruction_ability;
}
void Character::set_destruction_ability(unsigned int destruction_ability) {
  Character::destruction_ability = destruction_ability;
}

bool Character::operator==(const Character &character) const {
  return name == character.get_name() && surname == character.get_surname() &&
         description == character.get_description() &&
         level == character.get_level() && xp == character.get_xp() &&
         wage == character.get_wage() &&
         destruction_ability == character.get_destruction_ability();
}

std::ostream &operator<<(std::ostream &out, const Character &character) {
  out << "Character(" << character.get_name() << ", " << character.get_surname()
      << ")";

  return out;
}
const string &Character::get_avatar() const { return avatar; }
void Character::set_avatar(const string &avatar) { Character::avatar = avatar; }
}  // namespace characters
}  // namespace core
