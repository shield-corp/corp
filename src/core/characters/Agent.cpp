#include "Agent.h"
#include "../rules.h"

namespace core {
namespace characters {

unsigned int Agent::get_fighting_skill() const { return fighting_skill; }
void Agent::set_fighting_skill(unsigned int fighting_skill) {
  Agent::fighting_skill = fighting_skill;
}
unsigned int Agent::get_stealthness() const { return stealthness; }
void Agent::set_stealthness(unsigned int stealthness) {
  Agent::stealthness = stealthness;
}
unsigned int Agent::get_negociation() const { return negociation; }
void Agent::set_negociation(unsigned int negociation) {
  Agent::negociation = negociation;
}

std::ostream &operator<<(std::ostream &out, const Agent &agent) {
  out << "Agent( " << agent.get_name() << ", " << agent.get_surname() << ")";

  return out;
}

bool Agent::operator==(const Agent &agent) const {
  return (Character) * this == (Character)agent &&
         fighting_skill == agent.get_fighting_skill() &&
         stealthness == agent.get_stealthness() &&
         negociation == agent.get_negociation();
}

void Agent::set_stats() {
  switch (this->level) {
    case 1:
      set_xp_max(10);
      this->set_wage(10);
      this->set_fighting_skill(10);
      this->set_destruction_ability(10);
      this->set_stealthness(10);
      this->set_negociation(10);
      break;
    case 2:
      set_xp_max(20);
      this->set_wage(15);
      this->set_fighting_skill(20);
      this->set_destruction_ability(20);
      this->set_stealthness(15);
      this->set_negociation(20);
      break;
    case 3:
      set_xp_max(40);
      this->set_wage(20);
      this->set_fighting_skill(30);
      this->set_destruction_ability(30);
      this->set_stealthness(20);
      this->set_negociation(30);
      break;
    case 4:
      set_xp_max(80);
      this->set_wage(30);
      this->set_fighting_skill(40);
      this->set_destruction_ability(45);
      this->set_stealthness(25);
      this->set_negociation(35);
      break;
    case 5:
      set_xp_max(160);
      this->set_wage(40);
      this->set_fighting_skill(45);
      this->set_destruction_ability(50);
      this->set_stealthness(30);
      this->set_negociation(40);
      break;
    case 6:
      set_xp_max(320);
      this->set_wage(50);
      this->set_fighting_skill(50);
      this->set_destruction_ability(55);
      this->set_stealthness(35);
      this->set_negociation(45);
      break;
    case 7:
      set_xp_max(640);
      this->set_wage(70);
      this->set_fighting_skill(55);
      this->set_destruction_ability(60);
      this->set_stealthness(40);
      this->set_negociation(50);
      break;
    case 8:
      set_xp_max(1280);
      this->set_wage(90);
      this->set_fighting_skill(60);
      this->set_destruction_ability(65);
      this->set_stealthness(45);
      this->set_negociation(55);
      break;
    case 9:
      set_xp_max(2600);
      this->set_wage(120);
      this->set_fighting_skill(65);
      this->set_destruction_ability(70);
      this->set_stealthness(50);
      this->set_negociation(60);
      break;
    case 10:
      set_xp(0);
      set_xp_max(0);
      this->set_wage(150);
      this->set_fighting_skill(75);
      this->set_destruction_ability(80);
      this->set_stealthness(60);
      this->set_negociation(75);
      break;
    default:
      set_level(1);
      break;
  }
}
void Agent::level_update() {
  if (level < LEVEL_MAX) {
    if (xp >= xp_max) {
      xp = this->get_xp() - this->get_xp_max();
      this->set_level((this->level) + 1);
      this->set_stats();
    }
  }
}
unsigned int Agent::get_power() const {
  return get_stealthness() + get_negociation() + get_fighting_skill() +
         get_destruction_ability();
}
}  // namespace characters
}  // namespace core
