#include <boost/random/uniform_int.hpp>
#include <boost/random.hpp>

#include "Mission.h"
#include "Game.h"
#include "characters/character.h"
#include "rules.h"

namespace core {

void Mission::accept(character &character) {
  accepted = true;
  selected_character = character;
  start = Game::now;
  end = DateTime(Game::now.get_day() + duration.get_days());

  auto real_difficulty = random_between(0, to_real_difficulty(difficulty));

  if (auto hero = std::get_if<Hero>(&selected_character)) {
    if (real_difficulty < (hero->get_power())) {
      this->do_win();
    }
  } else if (auto agent = std::get_if<Agent>(&selected_character)) {
    if (real_difficulty < (agent->get_power())) {
      this->do_win();
    }
  }
}
const string &Mission::get_id() const {
  return id;
}
void Mission::set_id(const string &id) {
  Mission::id = id;
}
const string &Mission::get_title() const {
  return title;
}
void Mission::set_title(const string &title) {
  Mission::title = title;
}
const string &Mission::get_description() const {
  return description;
}
void Mission::set_description(const string &description) {
  Mission::description = description;
}
const string &Mission::get_objective() const {
  return objective;
}
void Mission::set_objective(const string &objective) {
  Mission::objective = objective;
}
unsigned int Mission::get_money_profit() const {
  return money_profit;
}
void Mission::set_money_profit(unsigned int money_profit) {
  Mission::money_profit = money_profit;
}
unsigned int Mission::get_xp() const {
  return xp;
}
void Mission::set_xp(unsigned int xp) {
  Mission::xp = xp;
}
const Duration &Mission::get_duration() const {
  return duration;
}
void Mission::set_duration(const Duration &duration) {
  Mission::duration = duration;
}
unsigned int Mission::get_difficulty() const {
  return difficulty;
}
void Mission::set_difficulty(unsigned int difficulty) {
  Mission::difficulty = difficulty;
}
unsigned int Mission::get_injury_probability() const {
  return injury_probability;
}
void Mission::set_injury_probability(unsigned int injury_probability) {
  Mission::injury_probability = injury_probability;
}
const Duration &Mission::get_injury_duration() const {
  return injury_duration;
}
void Mission::set_injury_duration(const Duration &injury_duration) {
  Mission::injury_duration = injury_duration;
}
const Region &Mission::get_location() const {
  return location;
}
void Mission::set_location(const Region &location) {
  Mission::location = location;
}
const DateTime &Mission::get_end() const {
  return end;
}
void Mission::set_end(const DateTime &end) {
  Mission::end = end;
}
const DateTime &Mission::get_start() const {
  return start;
}
void Mission::set_start(const DateTime &start) {
  Mission::start = start;
}
bool Mission::is_accepted() const {
  return accepted;
}
bool Mission::is_won() const {
  return win;
}
void Mission::do_win() {
  win = true;
}
bool Mission::is_finished() const {
  return is_accepted() && Game::now.get_day() > end.get_day();
}
bool Mission::operator==(const Mission &mission) const {
  return get_id() == mission.get_id();
}
std::ostream &operator<<(std::ostream &out, const Mission &mission) {
  out << "Mission( " << mission.get_id() << ", " << mission.get_title() << ")";

  return out;
}
const character &Mission::get_selected_character() const {
  return selected_character;
}
void Mission::set_selected_character(const character &selected_character) {
  Mission::selected_character = selected_character;
}
}  // namespace core
