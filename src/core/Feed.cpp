#include "Feed.h"

namespace core {
void Feed::push(string message) {
  messages.push_back(message);

  clean();
}

void Feed::clean() {
  while (messages.size() > capacity) {
    messages.pop_front();
  }
}
const int Feed::get_capacity() const {
  return capacity;
}
list<string> Feed::fetch_all() {
  return messages;
}
}
