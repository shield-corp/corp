#pragma once

#include <string>
#include <list>
#include <boost/shared_ptr.hpp>
using namespace std;

namespace core {
/**
 * Ensemble des messages affichés à l'utilisateur.
 */
class Feed {
 private:
  /**
   * Liste des messages.
   */
  list<string> messages;

  /**
   * Nombre maximale de messages contenu dans le `Feed`
   */
  const int capacity = 5;
  /**
   * Nettoye les anciens messages si la capacité est dépassé.
   */
  void clean();
 public:
  /**
   * Ajouter un message au `Feed`
   * @param message
   */
  void push(string message);
  /**
   * Récupère tous les messages.
   * @return
   */
  list<string> fetch_all();
  /**
   * Récupère la capacité du `Feed`
   * @return
   */
  const int get_capacity() const;
};
typedef boost::shared_ptr<Feed> spFeed;
}
