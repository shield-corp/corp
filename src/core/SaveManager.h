#pragma once

#include "Game.h"

#include <boost/filesystem/operations.hpp>
#include <string>

namespace fs = boost::filesystem;

using namespace std;

namespace core {
/**
 * Gestionnaire de sauvegardes.
 */
class Save {
 private:
  /**
   * Localisation du dossier.
   */
  fs::path path;
 public:
  /**
   * Fonction permettant de sauvegarder.
   * @param path
   */
  explicit Save(fs::path path);

  /**
   * Fonction permettant de récupérer le nom d'une sauvegarde.
   * @return le nom de la sauvegarde
   */
  const string get_name() const;

  /**
   * Fonction permettant de récupérer le chemin d'une sauvegarde.
   * @return le chemin de la sauvegarde
   */
  const fs::path get_path() const;
};

class SaveManager {
 private:
  /**
   * Chemin du dossier de sauvegarde.
   */
  fs::path directory;

 public:
  /**
   * Constructeur du gestionnaire de sauvegarde.
   * @param directory
   */
  explicit SaveManager(string &directory);

  /**
   * Renvoie le chemin du répertoire de sauvegardes.
   * @return le chemin du répertoire.
   */
  fs::path get_directory() const;

  /**
   * Créé une partie qui sera sauvegardée.
   * @param from
   * @return une partie.
   */
  spGame create(string from);

  /**
   * Charge une partie.
   * @param save
   * @return une partie.
   */
  spGame load(Save save);

  /**
   * Récupère les sauvegardes.
   * @return un tableau de sauvegardes.
   */
  vector<Save> get_saves();

  /**
   * Récupère le nombre de sauvegardes.
   * @return le nombre de sauvegardes.
   */
  unsigned int get_saves_length();
};
typedef boost::shared_ptr<SaveManager> spSaveManager;
}  // namespace core
