#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include "Region.h"

namespace core {
/**
 * Tableau regroupant l'ensemble des régions du jeu.
 */
class RegionRepository {
 private:
  /**
   * Tableau dynamique dans lequel sont stockées les régions
   */
  std::vector<Region> regions;
 public:
  /**
   * Fonction renvoyant un tableau de l'ensemble des régions.
   * @return l'ensemble des régions.
   */
  std::vector<Region> fetch_all() const;
  /**
   * Charge les régions depuis un fichier .xml .
   * @param ar
   */
  void load(cereal::XMLInputArchive &ar);
  /**
   * Insère une région.
   * @param region
   */
  void insert(Region &region);
};
typedef boost::shared_ptr<RegionRepository> spRegionRepository;
}  // namespace core
