/**
 * Fichier définissant les règles générales du jeu.
 */
#include <string>
using namespace std;
namespace core {
/**
 * Niveau minimal d'un personnage et d'une mission.
 */
const static int LEVEL_MIN = 1;

/**
 * Niveau maximal d'un personnage et d'une mission.
 */
const static int LEVEL_MAX = 10;

/**
 * Le nombre de missions déjà présente lors du lancement du jeu.
 */
const static int INITIAL_GENERATED_MISSIONS = 10;

/**
 * Nombre de missions générées par jour.
 */
const static int DAILY_GENERATED_MISSIONS = 1;

/**
 * Donne les chances d'un personnage de réussir une mission.
 * @param difficulty_level
 * @return un nombre définissant la valeur maximale du dé jeté pour la réussite/échec d'une mission.
 */
unsigned int to_real_difficulty(unsigned int difficulty_level);

/**
 * Palier de l'expérience gagnable en fonction du niveau (ex : lvl 1 = 5)
 */
const unsigned int EXPERIENCE_STEP[LEVEL_MAX] = {5, 10, 20, 30, 50,
                                                 100, 150, 200, 250, 350};

/**
 * Palier de l'argent gagnable en fonction du niveau (ex : lvl 1 = 15)
 */
const unsigned int MONEY_STEP[LEVEL_MAX] = {15, 20, 25, 35, 45,
                                            55, 75, 95, 125, 155};

}
