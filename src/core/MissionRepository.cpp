#include "MissionRepository.h"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/random.hpp>
#include <fstream>
#include "helpers.h"

using namespace std;

namespace fs = boost::filesystem;

namespace core {
vector<Mission> MissionRepository::fetch_all() const { return missions; };

void MissionRepository::insert(Mission mission) { missions.push_back(mission); }

bool MissionRepository::update(string id, Mission mission) {
  bool updated = false;

  for (auto &m : missions) {
    if (m.get_id() == id) {
      m = mission;
      updated = true;
    }
  }

  return updated;
}

bool MissionRepository::fetch_by_id(Mission &mission, string id) const {
  for (const auto &m : missions) {
    if (m.get_id() == id) {
      mission = m;
      return true;
    }
  }

  return false;
}

vector<Mission> MissionRepository::fetch_by_region(string region_id) const {
  vector<Mission> matching_missions;

  for (const auto &m : missions) {
    if (m.get_location().get_id() == region_id) {
      matching_missions.push_back(m);
    }
  }

  return matching_missions;
}

bool MissionRepository::destroy(string id) {
  for (auto mission = begin(missions); mission != end(missions); mission++) {
    if (mission->get_id() == id) {
      missions.erase(mission);
      // Utilistion du return car erased invalide iterator
      return true;
    }
  }

  return false;
}
void MissionRepository::load_file(string path) {
  std::ifstream file(path, ios::in);
  cereal::XMLInputArchive ar(file);

  if (strcmp(ar.getNodeName(), "mission") == 0) {
    Mission missioning;
    ar(missioning);

    missions.push_back(missioning);

  } else {
    throw Exception("Could not load file.");
  }
}
void MissionRepository::load(string path) {
  fs::path full_path(fs::initial_path<fs::path>());
  full_path = fs::system_complete(fs::path(path));
  if (!fs::exists(full_path)) {
    throw Exception("Folder not found.");
  }
  if (!fs::is_directory(full_path)) {
    throw Exception("Given path is not a directory.");
  }

  fs::directory_iterator end_iter;
  for (fs::directory_iterator dir_itr(full_path); dir_itr != end_iter;
       ++dir_itr) {
    if (fs::is_regular_file(dir_itr->status())) {
      load_file(dir_itr->path().c_str());
    }
  }
}
void MissionRepository::load_from_xml(cereal::XMLInputArchive &ar) {
  if (strcmp(ar.getNodeName(), "mission") == 0) {
    Mission mission;
    ar(mission);
    insert(mission);
  } else {
    throw Exception("Could not load file.");
  }
}
}  // namespace core
