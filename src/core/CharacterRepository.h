#pragma once

#include "characters/Agent.h"
#include "characters/Character.h"
#include "characters/Hero.h"
#include "characters/character.h"
#include "helpers.h"
#include "Mission.h"

#include <boost/variant.hpp>
#include <boost/shared_ptr.hpp>
#include <cereal/cereal.hpp>
#include <string>
#include <vector>
#include <variant>

using namespace std;

namespace core {
using namespace characters;
class CharacterRepository {
 /**
  * Représente les personnages de base sans type
  */
 private:
  /**
   * Tableau dynamique de personnage
   */
  vector<character> characters;

  /**
   * Charge les fichiers de héro
   * @param path
   */
  void load_file(string path);
 public:
  vector<character> fetch_all();
  vector<Agent> fetch_agents();
  vector<Hero> fetch_heros();
  unsigned int fetch_level_max();
  bool fetch_hero_by_name(Hero &hero, string name);
  bool fetch_agent_by_name(Agent &agent, string name);
  bool are_all_characters_injured();
  vector<int> get_level_occurrences();

  /**
   *
   * @param updated_character
   * @return true ou false
   */
  bool update(character &updated_character);

  /**
   * @brief permet de charger le dossier contenant les personnages
   * @param path
   */
  void load(string path);

  /**
    * @brief permet de charger l'ensemble des personnages via les fichiers xml
    * @param path
    */
  void load_from_xml(cereal::XMLInputArchive &ar);

  /**
   * @brief permet de calculer le salaire du personnage
   * @return le salaire du personnage
   */
  unsigned int fetch_wage_total();
};
typedef boost::shared_ptr<CharacterRepository> spCharacterRepository;
}  // namespace core
