#include "AgentItem.h"
#include "AgentDetailsPanel.h"
#include "CharacterEvent.h"
#include "res.h"
#include "GameScene.h"

void AgentItem::init(const Agent &agent) {
  setSize(getParent()->getWidth(), 200);

  this->agent = agent;
  const ResAnim *character = res::ui.getResAnim(agent.get_avatar());

  background = new Sprite;
  background->setResAnim(character);
  background->setSize(character->getSize());
  background->attachTo(this);
  background->setPosition(getWidth() / 2 - background->getWidth() / 2, 8);

  window = new Sprite;
  draw_border();
  window->setSize(border->getSize());
  window->attachTo(this);
  window->setPosition(getWidth() / 2 - window->getWidth() / 2, 0);

  TextStyle st;
  st.font = res::ui.getResFont("normal");
  st.vAlign = TextStyle::VALIGN_MIDDLE;
  st.hAlign = TextStyle::HALIGN_MIDDLE;
  st.color = Color(Color::DarkOrange);
  st.fontSize = 15;

  title = new TextField;
  title->attachTo(this);
  title->setText(agent.get_surname());
  title->setPosition(0, character->getHeight() + 25);
  title->setSize(getWidth(), 0);
  title->setStyle(st);

  addEventListener(TouchEvent::CLICK, CLOSURE(this, &AgentItem::clickHandler));
}

void AgentItem::clickHandler(Event *) {
  AgentEvent agent_event(AgentEvent::SELECTED, agent);
  getStage()->dispatchEvent(&agent_event);

  spAgentDetailsPanel agent_panel = new AgentDetailsPanel;
  agent_panel->init(agent);

  GameScene::instance->get_popup_manager()->popup(agent_panel);
}

void AgentItem::update(const UpdateState &us) {
  GameScene::instance->get_game()->get_character_repository()->fetch_agent_by_name(agent, agent.get_name());
  if (agent.get_available()) {
    background->setAlpha(255);
  } else {
    background->setAlpha(25);
  }
  draw_border();
}
void AgentItem::draw_border() {
  if (this->agent.get_level() == 1) {
    border = res::ui.getResAnim("character_border");
  } else if (this->agent.get_level() < 5) {
    border = res::ui.getResAnim("window_hero_bronze");
  } else if (this->agent.get_level() < 8) {
    border = res::ui.getResAnim("window_hero_silver");
  } else {
    border = res::ui.getResAnim("window_hero_gold");
  }
  window->setResAnim(border);
}