#include "InfoFeed.h"
#include "res.h"

void InfoFeed::init(spFeed &feed) {
  this->feed = feed;
  setSize(400, 120);

  spSprite background = new Sprite();
  background->setResAnim(res::ui.getResAnim("info_field_window"));
  background->attachTo(this);
  background->setSize(getSize());
  background->setPosition(0, 0);

  TextStyle body;
  body.font = res::ui.getResFont("normal");
  body.vAlign = TextStyle::VALIGN_MIDDLE;
  body.hAlign = TextStyle::HALIGN_LEFT;
  body.color = Color(Color::Black);
  body.fontSize = 14;

  for (int i = 0; i < feed->get_capacity(); i++) {

    spTextField text = new TextField;
    text->attachTo(this);
    text->setSize(0, 0);
    text->setStyle(body);

    fields.push_back(text);
  }
}

void InfoFeed::update(const UpdateState &us) {
  draw();
}

void InfoFeed::draw() {
  int y = 10;

  int i = 0;
  for (const auto &message : feed->fetch_all()) {
    spTextField text = fields.at(i);
    text->setPosition(15, 10 + y);
    text->setText(message);

    y += text->getHeight() + 20;
    i += 1;
  }
}
