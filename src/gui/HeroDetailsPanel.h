#pragma once

#include "../core/characters/Hero.h"
#include "oxygine-framework.h"

using namespace oxygine;
using namespace ::core::characters;

DECLARE_SMART(HeroDetailsPanel, spHeroDetailsPanel);

class HeroDetailsPanel : public Actor {
 private:
  spSprite exit;
  Hero hero;
  void onAdded2Stage() override;
  void onRemovedFromStage() override;
  const int MARGIN_LEFT = 40;
 public:
  void init(Hero &hero);

  void clickHandler(Event *E);
};
