#pragma once

#include "oxygine-framework.h"
#include "../core/MissionRepository.h"
#include "MissionPanelRegion.h"

using namespace oxygine;
using namespace ::core;

DECLARE_SMART(MissionPanelPage, spMissionPanelPage);

/**
 * Panneau contenant de l'ensemble des missions (taille illimitée), contenu dans un parent de taille limitée.
 */
class MissionPanelPage : public Actor {
 private:
  /**
   * Tableau des groupes de missions classés par régions.
   */
  vector<spMissionPanelRegion> regions;

  /**
   * Marge intérieure.
   */
  const int PADDING = 20;
 public:

  /**
   * Fonction affichant le panneau.
   */
  void draw();

  /**
   * Met à jour tous les groupes régionaux de missions et réaffiche le panneau.
   * @param us
   */
  void update(const UpdateState &us) override;

  /**
   * Initialise le panneau.
   * @param mission_repository
   * @param region_repository
   */
  void init(spMissionRepository &mission_repository, spRegionRepository &region_repository);
};
