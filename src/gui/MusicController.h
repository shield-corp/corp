#pragma once

#include "oxygine-framework.h"

using namespace oxygine;

DECLARE_SMART(MusicController, spMusicController);

/**
 * Gestionnaire de son.
 */
class MusicController : public Actor {
 protected:
  /**
   * Image du bouton d'activation/désactivation du son.
   */
  spSprite music;

  /**
   * Définit l'état du bouton en fonction de l'état du son.
   */
  void update_state();
 public:

  /**
   * Initialise le gestionnaire de son.
   */
  void init();

  /**
   * Active/désactive le son sur un évènement.
   * @param E
   */
  void toggle(Event *E);
};
