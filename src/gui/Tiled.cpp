#include "Tiled.h"

int Tiled::getWidth() {
  return width * tile_width;
}
int Tiled::getHeight() {
  return height * tile_height;
}

void Tiled::init(const std::string &tmx) {
  file::buffer fb;
  //read tmx into buffer
  file::read(tmx, fb);

  pugi::xml_document doc;
  // parse xml
  doc.load_buffer(fb.getData(), fb.getSize());

  pugi::xml_node map = doc.document_element();

  width = map.attribute("width").as_int();
  height = map.attribute("height").as_int();

  tile_width = map.attribute("tilewidth").as_int();
  tile_height = map.attribute("tileheight").as_int();

  setSize(width * tile_width, height * tile_height);

  //load layers
  pugi::xml_node layerNode = map.child("layer");
  while (!strcmp(layerNode.name(), "layer")) {
    pugi::xml_node data = layerNode.child("data");
    pugi::xml_node tile = data.first_child();

    layer l;
    l.tiles.reserve(width * height);

    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        l.name = layerNode.attribute("name").as_string();
        l.tiles.push_back(tile.attribute("gid").as_uint());
        tile = tile.next_sibling();
      }
    }

    layers.push_back(l);

    layerNode = layerNode.next_sibling();
  }

  pugi::xml_document tileset_doc;
  pugi::xml_node tileset;

  // load tilesets
  pugi::xml_node tileset_node = map.child("tileset");

  while (!strcmp(tileset_node.name(), "tileset")) {
    string source = tileset_node.attribute("source").value();

    file::read("../data/" + source, fb);

    tileset_doc.load_buffer(fb.getData(), fb.getSize());

    tileset = tileset_doc.document_element();

    pugi::xml_node tileset_tile = tileset.child("tile");

    spResAnim anim_frame = new ResAnim;

    while (!strcmp(tileset_tile.name(), "tile")) {
      anim_frame = res::ui.getResAnim(tileset_tile.child("properties").child("property").attribute("value").value());
      spSprite texture = new Sprite;
      texture->setAnimFrame(*anim_frame);
      texture->setWidth(anim_frame->getWidth());
      texture->setHeight(anim_frame->getHeight());

      textures.insert(make_pair(tileset_tile.attribute("id").as_uint(), texture));

      tileset_tile = tileset_tile.next_sibling();
    }

    tileset_node = tileset_node.next_sibling();
  }

  for (std::list<layer>::const_iterator i = layers.begin(); i != layers.end(); ++i) {
    drawLayer(*i);
  }
}

void Tiled::drawLayer(const layer &l) {
  const std::vector<unsigned int> &tiles = l.tiles;

  for (int y = 0; y < height; ++y) {
    for (int x = 0; x < width; ++x) {
      unsigned int tile = tiles[y * width + x];

      if (!tile)
        continue;

      tile -= 1;

      spSprite sprite = textures.at(tile);

      sprite->setPosition(x, y - sprite->getResAnim()->getHeight());

      addChild(sprite);
    }
  }
}