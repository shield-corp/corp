#pragma once

#include "oxygine-framework.h"
#include "../core/MissionRepository.h"

using namespace oxygine;
using namespace ::core;

DECLARE_SMART(MissionPanelRegion, spMissionPanelRegion);

/**
 * Groupe de mission contenu dans une région.
 */
class MissionPanelRegion : public Actor {
 private:

  /**
   * Le tableau de missions.
   */
  spMissionRepository mission_repository;

  /**
   * Un tableau associatif qui pour chaque mission associe son champ textuel.
   */
  map<string, spTextField> fields;

  /**
   * La région associée.
   */
  Region region;

  /**
   * Affiche le groupe de missions.
   */
  void draw();
 public:
  /**
   * Ajoute une mission au groupe.
   * @param mission
   */
  void add_mission(const Mission &mission);

  /**
   * Met à jour le groupe et le réaffiche.
   * @param us
   */
  void update(const UpdateState &us) override;

  /**
   * Initialise le groupe de missions.
   * @param mission_repository
   * @param region
   */
  void init(spMissionRepository &mission_repository, Region region);
};
