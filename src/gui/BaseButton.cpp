#include "BaseButton.h"
#include "Stage.h"

using namespace std;

namespace oxygine {
void BaseButton::copyFrom(const BaseButton &src, cloneOptions opt) {
  inherited::copyFrom(src, opt);

  _state = src._state;
  _resAnim = src._resAnim;
  _row = src._row;
}

BaseButton::BaseButton() : _state(NORMAL), _resAnim(0), _row(0), _btnPressed(0), _btnHovered(0) {
  EventCallback ncb = CLOSURE(this, &BaseButton::_mouseEvent);
  addEventListener(TouchEvent::TOUCH_DOWN, ncb);
  addEventListener(TouchEvent::OVER, ncb);
  addEventListener(TouchEvent::OUT, ncb);
  addEventListener(TouchEvent::CLICK, ncb);
}

BaseButton::~BaseButton() {
}

void BaseButton::_mouseEvent(Event *event) {
  if (_state == DISABLED) {
    return;
  }

  auto *me = safeCast<TouchEvent *>(event);
  if (event->type == TouchEvent::CLICK) {
    if (me->mouseButton == MouseButton_Left) {
      event->phase = Event::phase_target;
      event->target = this;
    } else {
      event->stopImmediatePropagation();
    }

    return;
  }

  if (me->mouseButton != MouseButton_Left)
    return;

  switch (event->type) {
    case TouchEvent::OVER: {
      if (!_btnHovered) {
        _btnHovered = me->index;
        if (!_btnPressed)
          setState(HOVERED);
      }
    }
      break;
    case TouchEvent::OUT: {
      if (_btnHovered == me->index) {
        if (!_btnPressed)
          setState(NORMAL);
        _btnHovered = 0;
      }
    }
      break;
    case TouchEvent::TOUCH_DOWN: {
      if (!_btnPressed) {
        _btnPressed = me->index;
        setState(PRESSED);
        addEventListener(TouchEvent::TOUCH_UP, CLOSURE(this, &BaseButton::_mouseEvent));
      }
    }
      break;
    case TouchEvent::TOUCH_UP: {
      if (_btnPressed == me->index) {
        setState(NORMAL);
        removeEventListener(TouchEvent::TOUCH_UP, CLOSURE(this, &BaseButton::_mouseEvent));
        _btnPressed = 0;
      }
    }
      break;
  }
}

void BaseButton::setResAnim(const ResAnim *r, int, int) {
  _resAnim = r;
  updateButtonState(_state);
}

void BaseButton::setRow(int row) {
  _row = row;
  updateButtonState(_state);
}

void BaseButton::setState(state s) {
  if (s == _state)
    return;
  _state = s;
  updateButtonState(s);
}

void BaseButton::updateButtonState(state s) {
  if (!_resAnim)
    return;

  if (_resAnim->getColumns() > s)
    inherited::setAnimFrame(_resAnim->getFrame(s, _row));
  else
    inherited::setAnimFrame(_resAnim->getFrame(0, _row));

}
void BaseButton::disable() {
  setState(DISABLED);
}
void BaseButton::enable() {
  setState(NORMAL);
}

}
