#pragma once

#include <oxygine-framework.h>
#include <oxygine-forwards.h>
#include <oxygine-include.h>

namespace oxygine {
DECLARE_SMART(BaseButton, spBaseButton);

class BaseButton : public Sprite {
 INHERITED(Sprite);
 public:
  DECLARE_COPYCLONE_NEW(BaseButton);

  BaseButton();
  ~BaseButton();

  int getRow() const { return _row; }

  void setResAnim(const ResAnim *r, int col = 0, int row = 0) override;

  /**Sets which row from ResAnim should be used. Default value is 0. Could be used for CheckBoxes*/
  void setRow(int row);

  void disable();

  void enable();

 protected:
  enum state {
    NORMAL,
    HOVERED,
    PRESSED,
    DISABLED
  };
  virtual void updateButtonState(state s);

  const ResAnim *_resAnim;
  int _row;
  state _state;

 private:
  pointer_index _btnPressed;
  pointer_index _btnHovered;

  void _mouseEvent(Event *event);

  void setState(state s);
};
}

EDITOR_INCLUDE(Button);
