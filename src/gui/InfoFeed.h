#pragma once

#include "../core/Feed.h"
#include "oxygine-framework.h"

using namespace oxygine;
using namespace ::core;

DECLARE_SMART(InfoFeed, spInfoFeed);

class InfoFeed : public ClipRectActor {
 private:
  spFeed feed;
  vector<spTextField> fields;
 public:
  void init(spFeed &feed);
  void update(const UpdateState &us);
  void draw();
};
