#include <iostream>
#include "MusicController.h"
#include "res.h"

void MusicController::init() {
  this->setSize(20, 20);

  music = new Sprite();
  music->attachTo(this);
  update_state();
  music->setSize(this->getSize());
  music->setPosition(0, 0);

  spSprite hitbox_music = new ColorRectSprite;
  hitbox_music->setColor(Color::Zero);
  hitbox_music->setSize(getSize());
  hitbox_music->setPosition(0, 0);
  hitbox_music->addClickListener(CLOSURE(this, &MusicController::toggle));
  hitbox_music->attachTo(this);
}

void MusicController::toggle(Event *E) {
  toggleSound();
  update_state();
}
void MusicController::update_state() {
  if (isPlayingSound()) {
    music->setResAnim(res::ui.getResAnim("music"));
  } else {
    music->setResAnim(res::ui.getResAnim("non_music"));
  }
}
