#include <string>

#include "../core/SaveManager.h"
#include "Menu.h"
#include "GameButton.h"
#include "GameScene.h"
#include "res.h"
#include "SaveLoadMenu.h"

void Menu::init() {
  setSize(400, 0);

  int y = 0;
  int const PADDING = 20;

  string save_directory = "../tmp/";
  spSaveManager save_manager = spSaveManager(new SaveManager(save_directory));

  spSprite logo = new Sprite;
  logo->setResAnim(res::ui.getResAnim("logo_pixel"));
  logo->setPosition(getWidth() / 2 - logo->getWidth() / 2, y);
  logo->attachTo(this);

  y += logo->getHeight() + PADDING;

  continue_button = new GameButton("Continuer");
  continue_button->setPosition(getWidth() / 2 - continue_button->getWidth() / 2, y);
  continue_button->attachTo(this);
  continue_button->addEventListener(TouchEvent::CLICK, [](Event *) {
    if (GameScene::instance) {
      flow::show(GameScene::instance);
    }
  });

  y += continue_button->getHeight() + PADDING;

  save_button = new GameButton("Sauvegarder");
  save_button->setPosition(getWidth() / 2 - continue_button->getWidth() / 2, y);
  save_button->attachTo(this);
  save_button->addEventListener(TouchEvent::CLICK, [](Event *) {
    if (GameScene::instance) {
      GameScene::instance->get_game()->save();
    }
  });

  y += save_button->getHeight() + PADDING;

  load_game_button = new GameButton("Charger une partie");
  load_game_button->setPosition(getWidth() / 2 - load_game_button->getWidth() / 2, y);
  load_game_button->attachTo(this);
  load_game_button->addEventListener(TouchEvent::CLICK, [save_manager](Event *) {
    SaveLoadMenu::instance = new SaveLoadMenu(save_manager);
    flow::show(SaveLoadMenu::instance);
  });

  y += load_game_button->getHeight() + PADDING;

  new_game_button = new GameButton("Nouvelle partie");
  new_game_button->setPosition(getWidth() / 2 - new_game_button->getWidth() / 2, y);
  new_game_button->attachTo(this);
  new_game_button->addEventListener(TouchEvent::CLICK, [save_manager](Event *) {
    spGame game = save_manager->create("../data");
    GameScene::instance = new GameScene(game);
    flow::show(GameScene::instance);
  });

  y += new_game_button->getHeight();

  setHeight(y);
  update_state();
}

void Menu::update_state() {
  if (!GameScene::instance) {
    continue_button->disable();
    save_button->disable();
  } else {
    continue_button->enable();
    save_button->enable();
  }
}
