#include "../../vendor/oxygine-freetype/src/ResFontFT.h"
#include "GameScene.h"
#include "MenuScene.h"
#include "oxygine-framework.h"
#include "oxygine-sound.h"
#include "core/oxygine.h"
#include "res.h"

using namespace oxygine;

SoundPlayer player;

void set_window_size() {
  SDL_DisplayMode dm;
  if (SDL_GetDesktopDisplayMode(0, &dm) == 0) {
    int w, h;
    int screenWidth = dm.w;
    int screenHeight = dm.h;

    if (screenWidth == 1920 && screenHeight == 1080) {
      w = 1800;
      h = 1000;
    } else if (screenWidth == 1600 && screenHeight == 900) {
      w = 1500;
      h = 800;
    } else {
      w = 960;
      h = 640;
    }

    SDL_SetWindowSize(oxygine::core::getWindow(), w, h);
    Point size = oxygine::core::getDisplaySize();
    getStage()->setSize(size);
  }
}

void set_icon(SDL_Window *window) {
  // this will "paste" the struct my_icon into this function
#include "../../data/images/icon.c"

  // these masks are needed to tell SDL_CreateRGBSurface(From)
  // to assume the data it gets is byte-wise RGB(A) data
  Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  int shift = (my_icon.bytes_per_pixel == 3) ? 8 : 0;
    rmask = 0xff000000 >> shift;
    gmask = 0x00ff0000 >> shift;
    bmask = 0x0000ff00 >> shift;
    amask = 0x000000ff >> shift;
#else // little endian, like x86
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = (icon.bytes_per_pixel == 3) ? 0 : 0xff000000;
#endif

  SDL_Surface *image = SDL_CreateRGBSurfaceFrom((void *) icon.pixel_data,
                                                icon.width,
                                                icon.height,
                                                icon.bytes_per_pixel * 8,
                                                icon.bytes_per_pixel * icon.width,
                                                rmask,
                                                gmask,
                                                bmask,
                                                amask);

  SDL_SetWindowIcon(window, image);

  SDL_FreeSurface(image);
}

void game_preinit() {}

void game_init() {
  set_window_size();
  set_icon(oxygine::core::getWindow());

  ResFontFT::initLibrary();
  ResFontFT::setSnapSize(10);

  SoundSystem::create()->init(4);

  // load resources
  res::load();

  flow::init();

  playSound("music");
  player.resume();
  // create all scenes
  MenuScene::instance = new MenuScene;
  GameScene::instance = nullptr;

  // show main menu
  flow::show(MenuScene::instance);
}

void game_update() {
  SoundSystem::get()->update();
  player.update();
  flow::update();
}

void game_destroy() {
  GameScene::instance = nullptr;
  MenuScene::instance = nullptr;

  res::free();
  ResFontFT::freeLibrary();

  SoundSystem::free();
  flow::free();
}