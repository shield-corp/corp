#pragma once

#include "oxygine-framework.h"
#include <map>
#include "res.h"

using namespace oxygine;
using namespace std;

DECLARE_SMART(Tiled, spTiled);

/**
 * Représente chaque affichage de pays sous forme de "tuile".
 */
class Tiled : public Actor {
 private:
  struct layer {
    /**
     * Nom de la tuile.
     */
    string name;

    /**
     * Tableau dynamique contenant les tuiles.
     */
    vector<unsigned int> tiles;
  };

  /**
   * Liste des couches d'affichage.
   */
  list<layer> layers;

  /**
   * Largeur du conteneur de tuiles.
   */
  int width;

  /**
   * Hauteur du conteneur de tuiles.
   */
  int height;

  /**
   * Tableau associatif de tuiles avec leur texture.
   */
  map<unsigned int, spSprite> textures;

  /**
   * Largeur de la tuile.
   */
  int tile_width;

  /**
   * Hauteur de la tuile.
   */
  int tile_height;

  /**
   * Dessine une couche.
   * @param l
   */
  void drawLayer(const layer &l);

 public:
  /**
   * Initialisation des tuiles.
   * @param tmx
   */
  void init(const std::string &tmx);
  int getWidth();
  int getHeight();
};
